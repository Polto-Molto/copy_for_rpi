#include "copystatusdialog.h"
#include "ui_copystatusdialog.h"

copyStatusDialog::copyStatusDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::copyStatusDialog)
{
    this->setWindowFlags(Qt::ToolTip);
    ui->setupUi(this);
}

copyStatusDialog::~copyStatusDialog()
{
    delete ui;
}

void copyStatusDialog::setFromFileName(QString str)
{
    ui->label_fromFile->setText(str);
}

void copyStatusDialog::setToFileName(QString str)
{
    ui->label_toFile->setText(str);
}

void copyStatusDialog::setProgressMaxValue(int v)
{
    ui->progressBar->setMaximum(v);
}

void copyStatusDialog::setProgressValue(int v)
{
    ui->progressBar->setValue(v);
}

void copyStatusDialog::on_pushButton_close_clicked()
{
    this->hide();
    this->reset();
}

void copyStatusDialog::reset()
{
    ui->label_fromFile->setText("");
    ui->label_toFile->setText("");
    ui->progressBar->setMaximum(1);
    ui->progressBar->setValue(0);
}
