#ifndef COPYSTATUSDIALOG_H
#define COPYSTATUSDIALOG_H

#include <QDialog>

namespace Ui {
class copyStatusDialog;
}

class copyStatusDialog : public QDialog
{
    Q_OBJECT

public:
    explicit copyStatusDialog(QWidget *parent = 0);
    ~copyStatusDialog();
public slots:
    void setFromFileName(QString);
    void setToFileName(QString);
    void setProgressMaxValue(int);
    void setProgressValue(int);

    void on_pushButton_close_clicked();

    void reset();

private:
    Ui::copyStatusDialog *ui;
};

#endif // COPYSTATUSDIALOG_H
