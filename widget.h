#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFileSystemModel>
#include <customtreeview.h>
#include <copystatusdialog.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
public slots:
    void openLeftDir(QModelIndex);
    void openRightDir(QModelIndex);

    void upLeftDir();
    void upRightDir();

    void mangaeCopyFiles(QStringList,QString);
    void collectFiles(QStringList,QString);
    void copyFiles();

    void delay( int );
private:
    Ui::Widget *ui;
    QFileSystemModel *leftModel;
    QFileSystemModel *rightModel;

    customTreeView *leftTree;
    customTreeView *rightTree;

    QModelIndex parentLeftIndex;
    QModelIndex parentRightIndex;

    copyStatusDialog *copyDlg;

    QStringList sourceFiles;
    QStringList distFiles;
};

#endif // WIDGET_H
