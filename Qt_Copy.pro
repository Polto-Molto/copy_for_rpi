#-------------------------------------------------
#
# Project created by QtCreator 2016-10-06T18:36:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qt_Copy
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    customtreeview.cpp \
    copystatusdialog.cpp
HEADERS  += widget.h \
    customtreeview.h \
    copystatusdialog.h
FORMS    += widget.ui \
    copystatusdialog.ui
