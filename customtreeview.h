#ifndef CUSTOMTREEVIEW_H
#define CUSTOMTREEVIEW_H

#include <QTreeView>

class customTreeView : public QTreeView
{
    Q_OBJECT
public:
    explicit customTreeView(QWidget *parent = 0);

signals:
    void SourceAndDist(QStringList,QString);
public slots:

protected:
    void dragMoveEvent(QDragMoveEvent *e);
    void dropEvent(QDropEvent *event);
};

#endif // CUSTOMTREEVIEW_H
