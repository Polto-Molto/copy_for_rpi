#include "customtreeview.h"
#include <QDebug>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QMimeData>

customTreeView::customTreeView(QWidget *parent) : QTreeView(parent)
{
    this->setAcceptDrops(true);
    this->setDragEnabled(true);
    this->setDragDropMode(QAbstractItemView::DragDrop);

}

void customTreeView::dragMoveEvent(QDragMoveEvent *e)
{
}

void customTreeView::dropEvent(QDropEvent *event)
{
   QStringList l = event->mimeData()->text().split("\n");
   QStringList list;
   foreach (QString str, l) {
        str.remove(0,7);
        list << str;
    }
   QModelIndex p;
   p = this->currentIndex();
   QString path = "/";
   while(this->model()->parent(p).isValid())
   {
       path.insert(1, p.data().toString()+"/");
       p = p.parent();
   }
   Q_EMIT SourceAndDist(list,path);
}
