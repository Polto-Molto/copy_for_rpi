#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QFileInfo>
#include <QFile>
#include <QTime>
#include <QThread>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    this->setWindowFlags(Qt::Tool);
    ui->setupUi(this);
    this->setAcceptDrops(true);

    copyDlg = new copyStatusDialog(this);

    leftTree = new customTreeView(this);
    rightTree = new customTreeView(this);

    leftModel = new QFileSystemModel;
    leftModel->setRootPath("/");
    leftTree->setModel(leftModel);
    parentLeftIndex = leftTree->currentIndex();

    leftTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
    leftTree->setDragEnabled(true);
    leftTree->setAcceptDrops(true);
    ui->gridLayout->addWidget(leftTree,1,0);


    rightModel = new QFileSystemModel;
    rightModel->setRootPath("/");
    rightTree->setModel(rightModel);
    parentRightIndex = rightTree->currentIndex();

    rightTree->setSelectionMode(QAbstractItemView::ExtendedSelection);
    rightTree->setDragEnabled(true);
    rightTree->setAcceptDrops(true);
    ui->gridLayout->addWidget(rightTree,1,1);


    connect(leftTree,SIGNAL(doubleClicked(QModelIndex))
            ,this,SLOT(openLeftDir(QModelIndex)));

    connect(rightTree,SIGNAL(doubleClicked(QModelIndex))
            ,this,SLOT(openRightDir(QModelIndex)));

    connect(leftTree,SIGNAL(SourceAndDist(QStringList,QString))
            ,this,SLOT(mangaeCopyFiles(QStringList,QString)));

    connect(rightTree,SIGNAL(SourceAndDist(QStringList,QString))
            ,this,SLOT(mangaeCopyFiles(QStringList,QString)));

    connect(ui->pbUpLeft,SIGNAL(clicked(bool))
            ,this,SLOT(upLeftDir()));

    connect(ui->pbUpRight,SIGNAL(clicked(bool))
            ,this,SLOT(upRightDir()));

}

Widget::~Widget()
{
    delete ui;
}
void Widget::mangaeCopyFiles(QStringList l,QString s)
{
    collectFiles(l,s);
    copyFiles();
}

void Widget::collectFiles(QStringList l,QString s)
{
    QDir dir;
    QString dist;
    foreach (QString str, l) {
       QFileInfo file_info(str);
       if(file_info.isDir())
       {
           dist = s+"/"+file_info.fileName()+"/";
           dir.mkdir(dist);
           dir.setPath(str);
           dir.setCurrent(str);
           QStringList ll  ;
           foreach (QString sss, dir.entryList()) {
               if(sss != "." && sss != "..")
                    ll << str+"/"+sss;
           }
           collectFiles(ll,dist);
           ll.clear();
       }
       else
       {
           sourceFiles << str;
           distFiles << s+file_info.fileName();
      }
    }
}

void Widget::copyFiles()
{
    copyDlg->show();
    copyDlg->setProgressMaxValue(sourceFiles.length()-1);
    for(int i =0 ; i < sourceFiles.length() ; i++)
    {
        copyDlg->setFromFileName(sourceFiles.at(i));
        copyDlg->setToFileName(distFiles.at(i));
        copyDlg->setProgressValue(i);
        QFile::copy(sourceFiles.at(i) , distFiles.at(i));
        delay(1);
    }
    distFiles.clear();
    sourceFiles.clear();
}

void Widget::openLeftDir(QModelIndex p)
{
    if(leftModel->isDir(p))
    {
        if((p.parent()).isValid())
            parentLeftIndex = p.parent();
        else
            parentLeftIndex = p;
        leftTree->setRootIndex(p);
    }
}

void Widget::openRightDir(QModelIndex p)
{
    if(rightModel->isDir(p))
    {
        if((p.parent()).isValid())
            parentRightIndex = p.parent();
        else
            parentRightIndex = p;
        rightTree->setRootIndex(p);
    }
}

void Widget::upLeftDir()
{
    leftTree->setRootIndex(parentLeftIndex);
        parentLeftIndex = parentLeftIndex.parent();
}

void Widget::upRightDir()
{
    rightTree->setRootIndex(parentRightIndex);
        parentRightIndex = parentRightIndex.parent();
}

void Widget::delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 1 );
    }
}
